# Theme
Change theme from CLI

# Usage
```console
usage: theme [theme-name] [--help] [--list]

Available themes:
  Light:
    cyprus
    day
    deuteranopia-light
    duo-light
    elea-light
    frost
    kassio
    light
    operandi
    spring
    summer
    tritanopia-light
    trio-light
  Dark:
    autumn
    bio
    cherie
    dark
    deuteranopia-dark
    duo-dark
    elea-dark
    night
    symbiosis
    tritanopia-dark
    trio-dark
    vivendi
    winter
```
